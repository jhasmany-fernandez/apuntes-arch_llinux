# UTILIDADES

## Gnome Shell:

```
sudo pacman -S gnome-browser-connector
```

## Bluetooth:

### Paso # 1. - Instalar recursos.

```
sudo pacman -S bluez bluez-utils
```

### Paso # 2. - Iniciar bluetooth.

```
sudo systemctl start bluetooth.service
```

### Paso # 3. - Habilitar junto al inicio del sistema.

```
sudo systemctl enable bluetooth.service
```

### Paso # 4. - Verificar el estatus.

```
sudo systemctl status bluetooth.service
```

## Warp:

### Paso # 1. - Descargar el archivo pkg.tar.zst

```
https://www.warp.dev/
```

### Paso # 2. - Instalar.

```
sudo pacman -U warp-terminal-v0.2024.08.20.08.02.stable_00-1-x86_64.pkg.tar.zst
```
