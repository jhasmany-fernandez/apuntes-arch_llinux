# NVIDIA

## Paso # 1. - Actualizar el sistema.

```
sudo pacman -Syu
```

## Paso # 2. - Activar los repositorios.

```
sudo pacman -S base base-devel
```

## Paso # 3. - Instalar git.

```
sudo pacman -S git
```

## Paso # 4. - Repositorio yay.

```
git clone https://aur.archlinux.org/yay.git
```

```
cd yay
```

```
makepkg -si
```

```
yay
```

## Paso # 5. - Instalar Nvidia.

```

```

## Paso # 6. - Verificacion instalacion nvidia.

```
lspci | grep -e VGA
```
