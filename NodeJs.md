# NODE JS

## Paso # 1. - installs fnm (Fast Node Manager)

```
curl -fsSL https://fnm.vercel.app/install | bash
```

## Paso # 2. - activate fnm

```
source ~/.bashrc
```

## Paso # 3. - source ~/.bashrc

```
fnm use --install-if-missing 20
```

## Paso # 4. - verifies the right Node.js version is in the environment

```
node -v # should print `v20.17.0`
```

## Paso # 5. - verifies the right npm version is in the environment

```
npm -v # should print `10.8.2`
```
