# NAVEGADORES

## Edge LTS:

### Paso # 1. - Clonar edge git.

```
https://aur.archlinux.org/microsoft-edge-stable-bin.git
```

```
cd microsoft-edge-stable-bin
```

### Paso # 2. - Instalar.

```
makepkg -si
```

## Chrome LTS:

### Paso # 1. - Clonar chrome git.

```
https://aur.archlinux.org/packages/google-chrome
```

### Paso # 2. - Instalar.

```
makepkg -si
```
