# DOCKER

## Paso # 1. - Instalar docker

```
sudo pacman -S docker docker-compose bash-completion
```

## Paso # 2. - Ver los estados systemctl.

### Ver estado.

```
sudo systemctl status docker
```

### Iniciar docker.

```
sudo systemctl start docker
```

### Inciar docker con el sistema

```
sudo systemctl start docker
```

### Verificar la instalacion docker

```
sudo docker version
```

### Correr un contenedor

```
sudo docker run hello-world
```

## Configurar Usuario para docker

### Verificar si estas en el grupo

```
cat /etc/group
```

### Agregar al grupo docker

```
sudo usermod -aG docker jhasmany
```

### Actualizar Grupo

```
newgrp docker
```

### Verificar en que grupos perteneces

```
groups
```
