# SPRING BOOT

## Paso # 1. - Instalar Open Jdk

```
https://aur.archlinux.org/dotnet-core-bin.git
```

## Paso # 2. - Instalar maven.

```
sudo pacman -S maven
```

## Paso # 3. - Instalar gradle.

```
sudo pacman -S gradle
```

## Paso # 4. - Instalar zip.

```
sudo pacman -S zip
```

## Paso # 5. - Instalar Sdk spring boot.

```
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
sdk install springboot
```

## Paso # 6. - Verificar las Instalacion.

```
mvn -v
gradle -v
spring --version
```
