# .NET

## Paso # 1. - Clonar .Net

```
https://aur.archlinux.org/dotnet-core-bin.git
```

```
cd android-studio
```

## Paso # 2. - Instalar.

```
makepkg -si
```

## Paso # 3. - Mono Instalar.

```
sudo pacman -S mono
```

## Paso # 4. - Msbuild Instalar.

```
sudo pacman -S msbuild
```

## Paso # 5. - Verificar las instalaciones.

```
mono --version
msbuild --version
```
