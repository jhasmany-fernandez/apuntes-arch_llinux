# VISUAL STUDIO CODE

## Instalar VSCode.

```
yay -S visual-studio-code-bin
```

## Extenciones:

```
makepkg -si
```

## Extenciones Failed:

```
Apc Extension failed: Error: EACCES: permission denied, copyfile '/home/jhasmany/.vscode/extensions/drcika.apc-extension-0.4.1/resources/bootstrap-amd.js' -> '/opt/visual-studio-code/resources/app/out/bootstrap-amd.js'
```

#### Solucion:

```
sudo chown -R $(whoami):$(whoami) /opt/visual-studio-code
```

```
sudo cp /home/jhasmany/.vscode/extensions/drcika.apc-extension-0.4.1/resources/bootstrap-amd.js /opt/visual-studio-code/resources/app/out/bootstrap-amd.js
```
